const std = @import("std");
const fs = std.fs;
const Allocator = std.mem.Allocator;
const ascii = std.ascii;

const Parser = struct {
    allocator: *Allocator,

    file: fs.File,
    store: std.ArrayList(u8),
    root: ?*Node,

    const Node = struct {
        body: []const u8,
        depts: std.ArrayList(*Node),

        fn findNode(self: *Node, body: []const u8) ?*Node {
            for (self.depts.items) |node| {
                if (std.mem.eql(u8, node.body, body)) {
                    return node;
                }

                if (node.findNode(body)) |child| {
                    return child;
                }
            }
            return null;
        }

        fn list(self: *Node, arr: *std.ArrayList(*Node)) UnexpectedError!void {
            for (self.depts.items) |node| {
                try node.list(arr);
            }

            for (arr.items) |n| {
                if (std.mem.eql(u8, n.body, self.body)) {
                    return;
                }
            }
            try arr.append(self);
        }
    };

    const UnexpectedError = std.os.ReadError || @TypeOf(std.AlignedArrayList(u8, null).ensureCapacity).ReturnType.ErrorSet;
    const FormatError = error{
        InvalidSyntax,
        WordTooLong,
        NotFound,
    };

    const Error = UnexpectedError || FormatError;

    const read_size: usize = 4096;

    fn init(allocator: *Allocator, file: fs.File) !*Parser {
        var parser: *Parser = try allocator.create(Parser);
        parser.* = Parser{
            .allocator = allocator,
            .file = file,
            .store = std.ArrayList(u8).init(allocator),
            .root = null,
        };

        return parser;
    }

    fn peek(self: *Parser, buff: []u8) Error!usize {
        var size: usize = buff.len;

        if (size > self.store.items.len) {
            var r: usize = 0;
            const left: usize = size - self.store.items.len;
            while (r < left) {
                try self.store.ensureCapacity(self.store.items.len + read_size + 1);
                r += try self.file.read(self.store.items.ptr[self.store.items.len .. self.store.items.len + read_size]);
                self.store.items.len += r;
            }
            size = self.store.items.len;
        }

        std.mem.copy(u8, buff, self.store.items[0..buff.len]);

        return size;
    }

    fn read(self: *Parser, buff: []u8) Error!usize {
        var size: usize = try self.peek(buff);
        for (self.store.items[0 .. self.store.items.len - size]) |*b, j| b.* = self.store.items[size + j];
        return size;
    }

    fn peekChar(self: *Parser) Error!u8 {
        var buff: [1]u8 = undefined;
        _ = try self.peek(buff[0..]);
        return buff[0];
    }

    fn readChar(self: *Parser) Error!u8 {
        var buff: [1]u8 = undefined;
        _ = try self.read(buff[0..]);
        return buff[0];
    }

    // consumeEmpty reads and discards all consecutive white spaces.
    fn consumeEmpty(self: *Parser) Error!void {
        var c: u8 = try self.peekChar();
        while (c == ' ' or c == '\t') : (c = try self.peekChar()) {
            _ = try self.readChar();
        }
    }

    // TODO accept an allocator for the word instead of just allocating and
    // leave it to the caller to handle the memory after
    fn readWord(self: *Parser) Error![]const u8 {
        var buff: [256]u8 = undefined;
        var allocator = &std.heap.FixedBufferAllocator.init(buff[0..]).allocator;

        var tmp_word = std.ArrayList(u8).init(allocator);
        var c: u8 = try self.peekChar();
        while (ascii.isAlNum(c) or c == '_') : (c = try self.peekChar()) {
            tmp_word.append(try self.readChar()) catch |err| switch (err) {
                Allocator.Error.OutOfMemory => return Error.WordTooLong,
                else => return err,
            };
        }

        const word: []u8 = try self.allocator.alloc(u8, tmp_word.items.len);
        std.mem.copy(u8, word, tmp_word.items);
        return word;
    }

    // expectNode reads in a word and returns it as the body of a Node.
    fn expectNode(self: *Parser) Error!?*Node {
        try self.consumeEmpty();

        var w = try self.readWord();
        if (w.len < 1) {
            return null;
        }

        if (self.findNode(w)) |n| {
            return n;
        }

        var n: *Node = try self.allocator.create(Node);
        n.* = Node{
            .body = w,
            .depts = std.ArrayList(*Node).init(self.allocator),
        };

        return n;
    }

    fn findNode(self: *Parser, body: []const u8) ?*Node {
        if (self.root == null) {
            return null;
        }
        return self.root.?.findNode(body);
    }

    // expectDepts parses the dependancies of a node as a white space seperated
    // list terminated with a newline
    fn expectDepts(self: *Parser, parent: *Node) Error!void {
        while (true) {
            try self.consumeEmpty();
            var word = try self.readWord();
            errdefer self.allocator.free(word);

            if (word.len < 1) {
                if (parent.*.depts.items.len < 1) {
                    return Error.InvalidSyntax;
                }

                try self.consumeEmpty();
                const c = try self.peekChar();
                if (c != '\n') {
                    return Error.InvalidSyntax;
                }

                return;
            }

            if (self.findNode(word)) |n| {
                try parent.*.depts.append(n);
                self.allocator.free(word);
            } else {
                var n: *Node = try self.allocator.create(Node);
                n.* = Node{
                    .body = word,
                    .depts = std.ArrayList(*Node).init(self.allocator),
                };
                try parent.*.depts.append(n);
            }
        }

        return;
    }

    fn run(self: *Parser) Error!void {
        try self.consumeEmpty();
        if (try self.expectNode()) |new| {
            self.root = new;
            try self.consumeEmpty();
            if ((try self.readChar()) != ':') {
                return Error.InvalidSyntax;
            }

            try self.consumeEmpty();
            try self.expectDepts(new);

            try self.consumeEmpty();
            if ((try self.readChar()) != '\n') {
                return Error.InvalidSyntax;
            }
        } else {
            return Error.InvalidSyntax;
        }

        while (true) {
            if (try self.expectNode()) |new| {
                try self.consumeEmpty();
                if ((try self.readChar()) != ':') {
                    return Error.InvalidSyntax;
                }

                try self.consumeEmpty();
                try self.expectDepts(new);

                try self.consumeEmpty();
                if ((try self.readChar()) != '\n') {
                    return Error.InvalidSyntax;
                }

                for (new.depts.items) |dept| {
                    if (std.mem.eql(u8, dept.body, self.root.?.body)) {
                        self.root = new;
                    }
                }

                continue;
            }

            return;
        }
    }

    const PrintError = UnexpectedError || std.os.WriteError;
    fn print(self: *Parser, out: std.fs.File) PrintError!void {
        var list = try std.ArrayList(*Node).initCapacity(self.allocator, 32);

        try self.root.?.list(&list);

        var count: usize = 0;
        for (list.items) |n| {
            count += 1;

            try out.outStream().print("{}. {}\n", .{ count, n.body });
        }
    }
};

pub fn main() anyerror!void {
    var allocator = std.heap.page_allocator;
    var f: fs.File = try fs.cwd().openFile("../input.txt", .{ .read = true });
    var p: *Parser = try Parser.init(allocator, f);

    try p.run();
    p.print(std.io.getStdOut()) catch unreachable;
}
