#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <ctype.h>

#define MAX_GRID 1024*1024
#define HEADER "P2"
#define HEADER_SIZE 2

#define ASCII_GREYSCALE " .:;+=%$#"
#define GREYSCALE_SIZE 8

void
put(uint64_t max, uint64_t scale)
{
	if (scale == 0) {
		putc(' ', stdout);
		return;
	}
	int idx = GREYSCALE_SIZE / (max / scale);
	putc(ASCII_GREYSCALE[idx], stdout);
}

void
consumeLine(FILE *file)
{
	while (fgetc(file) != '\n');
}

void
consumeComments(FILE *file)
{
	char c;
	while ((c = fgetc(file)) == '#') {
		consumeLine(file);
	}
	ungetc(c, file);
}

int main(int argc, char **argv) {
	FILE *file = NULL;
	char *buff = NULL;
	int r = 1;

	if (argc <= 1) {
		file = stdin;
	} else {
		if ((file = fopen(argv[1], "r")) == NULL) {
			perror(argv[1]);
			goto ret;
		}
	}

	uint64_t max = 0, x = 0, y = 0;

	size_t buff_size = BUFSIZ;
	buff = calloc(sizeof(char), BUFSIZ);
	if (!buff) {
		perror("mem");
		goto ret;
	}

	// read header
	if (getline(&buff, &buff_size, file) > 0) {
		if (strncmp(HEADER, buff, HEADER_SIZE)) {
			fprintf(stderr, "not a valid PGM file\n");
			goto ret;
		}
	}

	consumeComments(file);

	// read in the x and y values
	if (getline(&buff, &buff_size, file) > 0) {
		char *c = buff;
		while (*c != '\0' && *c != '\n') {
			switch (*c) {
			case ' ':
			case '\t':
				continue;
			default:
				if (!isalnum(*c)) {
					fprintf(stderr, "invalid PGM: unexpected char %c\n", *c);
					goto ret;
				}

				if (x != 0 && y != 0) {
					fprintf(stderr, "invalid PGM (x, y) definition\n");
					goto ret;
				}

				int n = atoi(c);
				if (x) {
					y = n;
				} else {
					x = n;
				}

				while (isalnum(*c)) c++;
			}

			c++;
		}
	} else {
		fprintf(stderr, "invalid PGM: unexpected EOF\n");
		goto ret;
	}

	if (getline(&buff, &buff_size, file) > 0) {
		char *c = buff;
		while (*c != '\0' && *c != '\n') {
			switch (*c) {
			case ' ':
			case '\t':
				continue;
			default:
				if (!isalnum(*c)) {
					fprintf(stderr, "invalid PGM: unexpected char %c\n", *c);
					goto ret;
				}

				int n = atoi(c);
				if (max) {
					fprintf(stderr, "invalid PGM\n");
					goto ret;
				} else {
					max = n;
				}
				while (isalnum(*c)) c++;
			}

			c++;
		}
	} else {
		fprintf(stderr, "invalid PGM: unexpected EOF\n");
		goto ret;
	}

	uint64_t at_x = 0;
	while (getline(&buff, &buff_size, file) > 0) {
		char *c = buff;
		bool is_comment = false;
		while (*c != '\0' && *c != '\n' && !is_comment) {
			switch (*c) {
			case ' ':
			case '\t':
				c++;
				continue;
			case '#':
				c++;
				is_comment = true;
				continue;
			default:
				if (!isdigit(*c)) {
					fprintf(stderr, "invalid PGM: unexpected char %c\n", *c);
					goto ret;
				}
				int n = atoi(c);

				put(max, n);

				while (isdigit(*c)) {
					c++ ;
				}
			}

			at_x++;
			if (at_x >= x) {
				putc('\n', stdout);
				at_x = 0;
			}
		}
	}

	printf("X: %d, Y: %d, MAX: %d\n", (int)x, (int)y, (int)max);

	r = 0;
ret:
	if (file) fclose(file);
	if (buff) free(buff);

	return r;
}
