package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

func main() {
	reader := bufio.NewReader(os.Stdin)

	cmd := os.Args[1]

	k := os.Args[2]
	key, err := strconv.Atoi(k)
	if err != nil {
		fmt.Fprintf(os.Stderr, "No integer was provided: %v\n", err)
		os.Exit(1)
	}
	if key <= 0 {
		fmt.Println("Error: you need to supply a key value of greater then 1")
		os.Exit(1)
	}

	fmt.Print("Enter text to encode: ")
	in, err := reader.ReadString('\n')
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error getting text to encode: %s\n", err)
	}
	in = strings.Replace(in, "\n", "", -1)

	if cmd == "encrypt" {
		fmt.Println(rot(in, key))
	} else if cmd == "decrypt" {
		fmt.Println(tor(in, key))
	} else {
		fmt.Printf("Error: %v is not a valid command. Valid commands are:\n", cmd)
		fmt.Println()
		fmt.Printf(" - encrypt\n")
		fmt.Printf(" - decrypt\n")
	}
}

func tor(s string, i int) string {
	r := math.Remainder(float64(i), 26)
	k := rune(r)
	var rs string
	for _, v := range s {
		switch {
		case v >= 65 && v <= 90:
			{
				v -= k
				if v < 65 {
					v = v + 26
				}
				rs += string(v)
			}
		case v >= 97 && v <= 122:
			{
				v -= k
				if v < 97 {
					v = v + 26
				}
				rs += string(v)
			}
		case v == 32:
			rs += string(v)
		}
	}
	return rs
}

func rot(s string, i int) string {
	r := math.Remainder(float64(i), 26)
	k := rune(r)
	var rs string
	for _, v := range s {
		switch {
		case v >= 65 && v <= 90:
			{
				v += k
				if v > 90 {
					v = v - 26
				}
				rs += string(v)
			}
		case v >= 97 && v <= 122:
			{
				v += k
				if v > 122 {
					v = v - 26
				}
				rs += string(v)
			}
		case v == 32:
			rs += string(v)
		}
	}
	return rs
}
