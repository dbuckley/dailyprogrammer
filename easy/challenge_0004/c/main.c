#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>

enum INPUTS {
	WITH_CAPITALS = 1 << 0,
	WITH_NUMBERS = 1 << 1,
	WITH_SYMBOLS = 1 << 2,
};

void help() {
	char *msg = "Usage: pass-gen [OPTIONS]\n"
		"\n"
		"Options:\n"
		"  -h\n    print this help message\n\n"
		"  -c\n    include capitle lettars\n\n"
		"  -n\n    include numbers\n\n"
		"  -s\n    include symbols\n\n"
		"  -l [NUMBER]\n    length of the password\n\n"
		"\n";
	printf(msg);
}

// Takes the WITH flag and sets str to the input returning the length
int input_string(int flag, char *str) {
	char *list = "abcdefghijklmnopqrstuvwxyz";
	int len = 26;

	char new_str[63];
	strncat(new_str, list, 26);

	if ((WITH_CAPITALS & flag)) {
		len += 26;

		char *caps="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		strncat(new_str, caps, 26);
	}
	if ((WITH_NUMBERS & flag)) {
		len += 10;

		char *num="0123456789";
		strncat(new_str, num, 10);
	}
	if ((WITH_SYMBOLS & flag)) {
		len += 26;

		char *sym="!@#$%^&*()[{]}\\|;:'\",<.>/?";
		strncat(new_str, sym, 26);
	}

	memcpy(str, new_str, len);
	return len;
}

char *passFromStr(int len, int str_len, char *str) {
	char *pass;
	pass = (char *) malloc(len);

   /* Intializes random number generator */
   time_t t;
   srand((unsigned) time(&t));

	int i;
	for (i = 0; i < len; i++) {
		int n = rand() % str_len;

		strncpy(pass+i, str+n, 1);
	}

	return pass;
}
	


int main(int argc, char *argv[]) {
	int input_types = 0;
	int len = 8;
	int opt = 0;
	while ((opt = getopt(argc, argv, "hcnsl:")) != -1) {
		switch (opt) {
		case 'h':
			help();
			return 0;
		case 'c':
			input_types = input_types | WITH_CAPITALS;
			break;
		case 'n':
			input_types = input_types | WITH_NUMBERS;
			break;
		case 's':
			input_types = input_types | WITH_SYMBOLS;
			break;
		case 'l':
			len = atoi( optarg );
			if (len == 0) {
				fprintf(stderr, "pass:gen: invalid number '%s' given for length", optarg);
				return 1;
			} else if (len < 0 || len > 3000) {
				fprintf(stderr, "pass:gen: length must be a positive number less that 3000");
				return 1;
			}
			break;
		case '?':
			if (optopt == 'l') {
				fprintf(stderr, "pass-gen: unexpected option '-%c'\n", optopt);
			} else if (isprint (optopt)) {
				fprintf (stderr, "pass-gen: unknown option `-%c'.\n", optopt);
			} else {
				fprintf (stderr, "pass-gen: unkown option character `\\x%x'.\n", optopt);
			}
			return 1;
		default:
			abort();
		}
	}

	char str[100];
	int str_len = input_string(input_types, str);

	char *pass = passFromStr(len, str_len, str);
	printf("Your password is: %s\n", pass);
}
