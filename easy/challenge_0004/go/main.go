package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	var s string
	var err error
	if len(os.Args) == 2 {
		s = os.Args[1]
	} else if len(os.Args) < 2 {
		reader := bufio.NewReader(os.Stdin)

		fmt.Print("Length of password: ")
		s, err = reader.ReadString('\n')
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error reveiving text from console: %s\n", err)
			return
		}
		s = strings.Trim(s, "\n")
	} else {
		fmt.Fprintf(os.Stderr, "Error only a single int is accepted as an argument. Using %s and ignoring all others.\n", os.Args[1])
		s = os.Args[1]
	}

	n, err := strconv.Atoi(s)
	if err != nil {
		fmt.Fprintf(os.Stderr, "An integer was expected: %s\n", err)
		return
	}

	sample := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_=+[{}]|;:,<.>/?")

	fmt.Println(randSample(n, sample))
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func randSample(x int, r []rune) string {
	b := make([]rune, x)
	for i := 0; i < x; i++ {
		b[i] = r[rand.Intn(len(r))]
	}
	return string(b)
}
