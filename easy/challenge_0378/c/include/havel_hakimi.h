#ifndef _HAVEL_HAKIMI_H
#define _HAVEL_HAKIMI_H

#include <stddef.h>

int
hh_check(const int *input, size_t len);

#endif /* _HAVEL_HAKIMI_H */
