#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <havel_hakimi.h>

struct test {
	int input[128 + 1];
	size_t input_n;
	int expect;
};

struct test tests[100] = {
	{ .input = {0}, .input_n = 1, .expect = 1 },
	{ .input = {0}, .input_n = 3, .expect = 1 },
	{ .input = {0,0,3}, .input_n = 3, .expect = 0 },
	{ .input = {5, 3, 0, 2, 6, 2, 0, 7, 2, 5}, .input_n = 10, .expect = 0 },
	{ .input = {4, 2, 0, 1, 5, 0}, .input_n = 6, .expect = 0 },
	{ .input = {3, 1, 2, 3, 1, 0}, .input_n = 6, .expect = 1 },
	{ .input = {16, 9, 9, 15, 9, 7, 9, 11, 17, 11, 4, 9, 12, 14, 14, 12, 17, 0, 3, 16}, .input_n = 20, .expect = 1 },
	{ .input = {14, 10, 17, 13, 4, 8, 6, 7, 13, 13, 17, 18, 8, 17, 2, 14, 6, 4, 7, 12}, .input_n = 20, .expect = 1 },
	{ .input = {15, 18, 6, 13, 12, 4, 4, 14, 1, 6, 18, 2, 6, 16, 0, 9, 10, 7, 12, 3}, .input_n = 20, .expect = 0 },
	{ .input = {6, 0, 10, 10, 10, 5, 8, 3, 0, 14, 16, 2, 13, 1, 2, 13, 6, 15, 5, 1}, .input_n = 20, .expect = 0 },
};

int
main()
{
	assert(hh_check(NULL, 1) == 1);

	for (int i = 0; tests[i].input_n != 0; i++) {
		printf("Testing: %i\n", i);
		assert(hh_check(tests[i].input, tests[i].input_n) == tests[i].expect);
	}
}
