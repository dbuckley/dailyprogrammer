#include <stdlib.h>
#include <stdio.h>

#include <havel_hakimi.h>


int
cmp(const void *a, const void *b) {
	return (*(int*)b-*(int*)a);
}


int
hh_check(const int *input, size_t len)
{
	if (input == NULL)
		return 1;

	int *clean = calloc(len, sizeof(int));
	if (clean == NULL)
		return 0;

	size_t n = 0;
	for (size_t i = 0; i < len; i++) {
		int v = input[i];
		printf("Checking val: %i\n", v);
		if (v != 0) {
			clean[n] = v;
			n++;
		}
	}

	len = n;

	int ret = 1;

	if (!len) {
		ret = 1;
		goto out;
	}

	qsort(clean, n, sizeof(int), cmp);

	int N = clean[0];
	if (N > len - 1) {
		ret = 0;
		goto out;
	}

	for (int j = 0; j < N; j++) {
		if (j+1 >= n)
			break;
		if (clean[j+1] == 0)
			continue;
		clean[j+1]--;
	}

	printf("Recursing with array start of %i and length of %i\n", *(clean+1), (int)(n-1));
	ret = hh_check(clean+1, n-1);

out:
	free(clean);
	return ret; 
}
