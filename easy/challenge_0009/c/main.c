#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <ctype.h>

unsigned int SORT_MODE = 0; // Reverse comparison

// Modes:
static const unsigned int SORT_REVERSE = 1 << 0; // Reverse comparison
// static const unsigned int SORT_WOBLANKS = 1 << 1; // Ignore leading blanks

struct LineBuff {
	int line_len;
	int cap;
	char **lines;
};

struct LineBuff *newLineBuff() {
	struct LineBuff *buff = malloc(sizeof(struct LineBuff));
	if (buff == NULL) {
		return NULL;
	}

	buff->lines = malloc(32 * sizeof(char *) + 1);
	buff->line_len = 0;
	buff->cap = 32;

	return buff;
}

void destroy_line_buff(struct LineBuff *buff) {
	int i;
	for (i = 0; i < buff->line_len; ++i) {
		free(buff->lines[i]);
	}
	free(buff->lines);
	free(buff);
}

int addLine(struct LineBuff *buff, char *line) {
	if (buff->line_len == buff->cap) {
		int cap = buff->cap * 2;
		char **old_lines;
		old_lines = buff->lines;
		buff->lines = malloc(cap * sizeof(char*) + 1);
		int i = 0;
		for (; i < buff->cap; i++) {
			buff->lines[i] = old_lines[i];
		}
		buff->cap = cap;
		free(old_lines);
	}

	buff->lines[buff->line_len] = line;
	buff->line_len++;

	return 0;
}

int buffFromFile(struct LineBuff *buff, char *path) {
	FILE *strm;
	if (path[0] == '-' && path[1] == '\0') {
		strm = stdin;
	} else {
		strm = fopen(path, "r");
	}

	if (strm == NULL) {
		perror("sort");
	}

	char str[100];
	while (fgets(str, 100, strm)) {
		int str_len = strlen(str) + 1;

		char *line = malloc(str_len*sizeof(char));
		memcpy(line, str, str_len);
		addLine(buff, line);
	}

	return fclose(strm);
}

int alphaSort(const void *a, const void *b) {
	char *a_str = *(char **)a;
	char *b_str = *(char **)b;

	if (SORT_MODE & SORT_REVERSE) {
		return 0 - strcoll(a_str, b_str);
	}

	return strcoll(a_str, b_str);
}

void sort(struct LineBuff *buff) {
	qsort(buff->lines, buff->line_len, sizeof(char *), alphaSort);
}


int main(int argc, char *argv[]) {
	bool uniq = false;
	// bool check = false;

	char opt;
	while ((opt = getopt(argc, argv, "urc")) != -1) {
		switch (opt) {
		case 'c':
			// check = true;
			break;
		case 'u':
			uniq = true;
			break;
		case 'r':
			SORT_MODE |= SORT_REVERSE;
			break;
		}
	}


	struct LineBuff *buff = newLineBuff();
	if (buff == NULL) {
		fprintf(stderr, "sort: memory error\n");
	}

	if (optind == argc) {
		buffFromFile(buff, "-");
	} else {
		int i;
		for (i = optind; i < argc; ++i) {
			char *path;

			path = argv[i];
			if (buffFromFile(buff, path) != 0) {
				perror("read");
			}
		}
	}

	sort(buff);

	int l;
	for (l = 0; l < buff->line_len; ++l) {
		if ( uniq && l > 0 && strcmp(buff->lines[l], buff->lines[l-1]) == 0) {
			continue;
		}
		fputs(buff->lines[l], stdout);
	}

	destroy_line_buff(buff);
	return 0;
}
