package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
)

func main() {
	s := bufio.NewScanner(os.Stdin)
	if len(os.Args) < 1 {
		io.WriteString(os.Stderr, "Please specify either \"tax\" or \"overall\"\n")
		os.Exit(1)
	}
	switch os.Args[1] {
	case "tax":
		for s.Scan() {
			val, err := strconv.Atoi(s.Text())
			if err != nil {
				io.WriteString(os.Stderr, "(ignoring) expected a whole number: "+err.Error()+"\n")
				continue
			}
			tax := tax(examplaniaTB, val)
			io.WriteString(os.Stdout, strconv.Itoa(tax)+"\n")
		}
	case "overall":
		for s.Scan() {
			val, err := strconv.ParseFloat(s.Text(), 64)
			if err != nil {
				io.WriteString(os.Stderr, "(ignoring) expected a decimal: "+err.Error()+"\n")
				continue
			} else if val > 1 {
				io.WriteString(os.Stderr, "(ignoring) must be a percantage less than 1\n")
				continue
			}
			tax, err := overall(examplaniaTB, val)
			if err != nil {
				io.WriteString(os.Stderr, "(ignoring) failed to get overall: "+err.Error()+"\n")
				continue
			}
			io.WriteString(os.Stdout, strconv.Itoa(tax)+"\n")
		}
	}
}

type taxBracket []struct {
	incomeCap    int
	aboveTaxRate float64
}

var examplaniaTB = taxBracket{
	{incomeCap: 100000, aboveTaxRate: .40},
	{incomeCap: 30000, aboveTaxRate: .25},
	{incomeCap: 10000, aboveTaxRate: .10},
}

func tax(tb taxBracket, i int) int {
	var f float64 = 0
	for _, tx := range tb {
		if i <= tx.incomeCap {
			continue
		}
		f += float64(i-tx.incomeCap) * tx.aboveTaxRate
		i = tx.incomeCap
	}
	return int(math.Floor(f))
}

func overall(tb taxBracket, o float64) (int, error) {
	base := 0
	var max int = 2147483647
	if o > tb[0].aboveTaxRate {
		return 0, errors.New(fmt.Sprintf("cannot get overall when percentage is greater then the highest bracket (%d)", tb[0].aboveTaxRate))
	}

	for base <= max {
		mid := int(math.Floor(float64(base+max) / 2))
		cRaw := float64(tax(tb, mid)) / float64(mid)
		c := math.Floor(cRaw*1000) / 1000
		switch {
		case c < o:
			base = mid + 1
		case c > o:
			max = mid - 1
		default:
			return mid, nil
		}
	}

	return 0, errors.New("failed to find a overall")
}
