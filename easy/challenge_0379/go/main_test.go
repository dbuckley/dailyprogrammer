package main

import (
	"fmt"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestGetTax_Explania(t *testing.T) {
	tTable := []struct {
		income int
		tax    int
	}{
		{income: 0, tax: 0},
		{income: 10000, tax: 0},
		{income: 10009, tax: 0},
		{income: 10010, tax: 1},
		{income: 12000, tax: 200},
		{income: 56789, tax: 8697},
		{income: 1234567, tax: 473326},
	}

	for _, ts := range tTable {
		t.Run(fmt.Sprintf("income of %d gives tax of %d", ts.income, ts.tax), func(t *testing.T) {
			got := tax(examplaniaTB, ts.income)
			if got != ts.tax {
				t.Error(cmp.Diff(ts.tax, got))
			}
		})
	}
}
