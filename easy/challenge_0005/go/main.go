package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	users, err := FromFile("users.txt")
	if err != nil {
		fmt.Fprintf(os.Stderr, "go-auth: failed to get users: %v\n", err)
		os.Exit(1)
	}

	user := mustRead("Username: ")
	pass := mustRead("Password: ")
	if users.auth(user, pass) {
		fmt.Println("Authenticated successfully!")
	} else {
		fmt.Println("Unauthorized!")
	}
}

type users map[string]string

func FromFile(file string) (users, error) {
	u := make(users)
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}

	for {
		var user string
		var pass string
		_, err := fmt.Fscanln(f, &user, &pass)
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		u[user] = pass
	}

	return u, nil
}

func (u users) auth(user, pass string) bool {
	for usr, ps := range u {
		if usr == user {
			return ps == pass
		}
	}
	return false
}

func mustRead(msg string) string {
	reader := bufio.NewReader(os.Stdin)
	var str string
	for str == "" {
		fmt.Printf(msg)
		raw_str, _, err := reader.ReadLine()
		if err != nil {
			panic(fmt.Errorf("go-auth: failed to read from stdin: %v", err))
		}
		str = strings.ReplaceAll(string(raw_str), "\n", "")
	}

	return str
}
