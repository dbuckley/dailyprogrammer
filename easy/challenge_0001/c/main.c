#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int getsInt(char *msg, FILE *stream) {
    int n = 0;
    char n_str[100];

    while (n == 0) {
        printf("%s", msg);
		fgets(n_str, 100, stream);
		n = atoi(n_str);
		if (n == 0) {
			printf("error: not a valid number!\n");
		}
    }

    return n;
}

void trimNewline(char *str) {
	int i, gap;
	size_t len, new_len;

	len = strlen(str);

	gap = 0;
	for (i = 0; i < len; i++) {
		if (str[i] == '\0') {
			break;
		}

		if (str[i] == '\n' ) {
			str[i] = '\0';
			break;
		}
	}
}

int main() {
    char name[100];
    char uname[100];
    int age;
    
    printf("Enter your name: ");
    fgets(name, 100, stdin);
    trimNewline(name);
    
    age = getsInt("Enter your age: ", stdin);

    printf("Enter your reddit.com username: ");
    fgets(uname, 100, stdin);
    trimNewline(uname);
    
    printf("\nHello \"%s\", you are \"%d\" years old and your username is \"%s\".\n", name, age, uname);
}
