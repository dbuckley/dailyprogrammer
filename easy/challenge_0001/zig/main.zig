const std = @import("std");
const c = @cImport({
    @cInclude("stdio.h");
});
const io = std.io;

pub fn main() !void {
    var bytes: [128]u8 = undefined;
    const allocator = &std.heap.FixedBufferAllocator.init(bytes[0..]).allocator;

    var name = try prompt(allocator, "Hello, what is your name?\n");
    var age = try prompt(allocator, "And your age?\n");

    var all_together: [100]u8 = undefined;

    // You can use slice syntax on an array to convert an array into a slice.
    // const all_together_slice = all_together[0..];
    const out = try std.fmt.bufPrint(all_together[0..], "Your name is {}, and you are {} years old.\n", name, age);

    var stdout_file = try io.getStdOut();
    try stdout_file.write(out);
}

fn prompt(aloc: *std.mem.Allocator, msg: []const u8) ![]const u8 {
    var stdout_file = try io.getStdOut();
    try stdout_file.write(msg);

    var buf = try std.Buffer.initSize(aloc, 0);
    return std.io.readLine(&buf);
}
