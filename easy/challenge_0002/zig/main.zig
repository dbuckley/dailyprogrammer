const std = @import("std");
const c = @cImport({
    @cInclude("stdio.h");
});
const io = std.io;
const os = std.os;
const mem = std.mem;
const fmt = std.fmt;

const force = struct {
    force: f64,
    mass: f64,
    accel: f64,

    fn init(f: f64, m: f64, a: f64) force {
        return force{
            .force = f,
            .mass = m,
            .accel = a,
        };
    }

    fn calcAccel(self: force) f64 {
        return self.force / self.mass;
    }

    fn calcMass(self: force) f64 {
        return self.force / self.accel;
    }

    fn calcForce(self: force) f64 {
        return self.mass * self.accel;
    }
};

fn printHelp() !void {
    var stderr = io.getStdErr();
    const msg =
        \\Usage: fma_calc [force|mass|accel]
        \\
        \\Overview: fma_calc is used to calculate force, mass, or acclereration with the needed
        \\input
    ;
    try stderr.write(msg);
}

pub fn main() !void {
    // Setup allocator for the cli
    var arena = std.heap.ArenaAllocator.init(std.heap.direct_allocator);
    defer arena.deinit();
    const allocator = &arena.allocator;

    if (os.argv.len < 2) {
        try printHelp();
        os.exit(1);
    }

    // var answer: []const u8 = undefined;
    const stdout_file = io.getStdOut();
    const stdout = &stdout_file.outStream().stream;

    const opt: []const u8 = mem.toSlice(u8, os.argv[1]);
    if (mem.eql(u8, opt, "h") or mem.eql(u8, opt, "help")) {
        try printHelp();
    } else if (mem.eql(u8, opt, "force")) {
        var m = try parseInt(try prompt(allocator, "Mass: "));
        var a = try parseInt(try prompt(allocator, "Acceleration: "));
        var fma = force.init(0, m, a);
        var f = fma.calcForce();

        try stdout.print("{}\n", .{f});
    } else if (mem.eql(u8, opt, "mass")) {
        var f = try parseInt(try prompt(allocator, "Force: "));
        var a = try parseInt(try prompt(allocator, "Acceleration: "));
        var fma = force.init(f, 0, a);
        var m = fma.calcMass();

        try stdout.print("{}\n", .{m});
    } else if (mem.eql(u8, opt, "accel")) {
        var f = try parseInt(try prompt(allocator, "Force: "));
        var m = try parseInt(try prompt(allocator, "Mass: "));
        var fma = force.init(f, m, 0);
        var a = fma.calcAccel();

        try stdout.print("{}\n", .{a});
    } else {
        try stdout.print("invalid input\n", .{});
        try printHelp();
        os.exit(1);
    }
}

fn prompt(aloc: *std.mem.Allocator, msg: []const u8) ![]const u8 {
    var stdout_file = io.getStdOut();
    try stdout_file.write(msg);

    var buf = try std.Buffer.initSize(aloc, 0);
    return std.io.readLine(&buf);
}

fn parseInt(in: []const u8) !f64 {
    if (std.fmt.parseFloat(f64, in)) |val| {
        return val;
    } else |err| {
        return err;
    }
}
