// This is a program to calculate the relationship of
// force mass and accceleration.
package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func usage() {
	fmt.Print(`Usage: fma [m|f|a|help]

`)
}

func warn(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "fma: "+format, args...)
}

func fail(format string, args ...interface{}) {
	warn(format, args...)
	os.Exit(1)
}

func main() {
	if len(os.Args) != 2 {
		fail("unexpected number of arguments given\n")
	}

	switch strings.ToLower((os.Args[1])) {
	case "-h":
		fallthrough
	case "--help":
		fallthrough
	case "h":
		fallthrough
	case "help":
		usage()
		os.Exit(0)
	case "f":
		fallthrough
	case "force":
		a := mustGet("Acceleration")
		m := mustGet("Mass")

		f := m * a
		fmt.Printf("The resultant force is: %f\n", f)
	case "m":
		fallthrough
	case "mass":
		a := mustGet("Acceleration")
		f := mustGet("Force")

		m := f / a
		fmt.Printf("The resultant force is: %f\n", m)
	case "a":
		fallthrough
	case "acceleration":
		f := mustGet("Force")
		m := mustGet("Mass")

		a := f / m
		fmt.Printf("The resultant force is: %f\n", a)
	default:
		warn("unexpected argument %q\n\n", os.Args[1])
		usage()
		os.Exit(0)
	}

}

func mustGet(s string) float64 {
	reader := bufio.NewReader(os.Stdin)
	fmt.Fprintf(os.Stderr, "%s: ", s)

	var i float64 = 0
	for i == 0 {
		rs, err := reader.ReadString('\n') // If this fails there are bigger issues
		rs = strings.Replace(rs, "\n", "", -1)
		if err != nil {
			fail("failed to read from stdin")
		}

		i, err = strconv.ParseFloat(rs, 64)
		if err != nil {
			i = 0
			warn("%q is not a valid int\n", rs)
			fmt.Fprintf(os.Stderr, "%s: ", s)
		} else {
			break // Accept a 0 for input
		}
	}

	return i
}
