{
  description = "Devshell for r/dailprogramming";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/master";
    flake-utils.url = "github:numtide/flake-utils";
    zigflk = {
      url = "git+https://git.sr.ht/~dbuckley/zig-flake";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, ... }@inputs:
    inputs.flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system}.extend inputs.zigflk.overlay.${system};
      in
      {

        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            pkg-config

            # Go
            go
            gopls
            gotools

            # C
            meson
            ninja
            glibc
            gcc
            check
            ccls

            # Zig
            zig_master
            zls

            # Build Tools
            pkg-config
            valgrind
            entr
            gdb

            # C Libs
            editline
            libpng
            zlib
          ];
        };
      });
}
